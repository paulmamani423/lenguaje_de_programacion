//a. Realiza el mismo algoritmo utilizando Mientras (While); también hazlo utilizando En Caso (Case)

Algoritmo ejercicio7
    Definir Nota como Entero; Definir NuevaNota como Car�cter;
	
    Escribir "ingrese la calificacion numerica (entre 1 y 20):"
    Leer Nota
	Mientras (Nota < 1 O Nota > 20) Hacer;
        Escribir "la calificacion ingresada no es valida. Ingrese una calificacion entre 1 y 20:"
        Leer Nota
    Fin Mientras
    Segun Nota Hacer
        19, 20:
            NuevaNota <- "A"
        16, 17, 18:
            NuevaNota <- "B"
        13, 14, 15:
            NuevaNota <- "C"
        10, 11, 12:
            NuevaNota <- "D"
        1, 2, 3, 4, 5, 6, 7, 8, 9:
            NuevaNota <- "E"
    Fin Segun
    Escribir "la calificacion convertida es:", NuevaNota
Fin Algoritmo