//Determine el resultado del siguiente diagrama de flujo, realice el pseudocódigo. Elabore su enunciado.

Algoritmo ejercicio16
    Definir N, BC, BV, BD, BC2, BM, Resto, C, Total Como Entero;
	
    BC <- 0; BV <- 0; BD <- 0; BC2 <- 0; BM <- 0;
	
    Escribir "ingrese la cantidad en efectivo:";
    Leer N
    C <- N
    Si C >= 50000 Entonces;
        BC <- BC + 1
        C <- C - 50000
    FinSi
    Si C >= 20000 Entonces;
        BV <- BV + 1
        C <- C - 20000
    FinSi
    Si C >= 10000 Entonces;
        BD <- BD + 1
        C <- C - 10000
    FinSi
    Si C >= 5000 Entonces;
        BC2 <- BC2 + 1
        C <- C - 5000
    FinSi
    Si C >= 1000 Entonces;
        BM <- BM + 1
        C <- C - 1000
    FinSi
    Resto <- C
    Total <- N - Resto
    Escribir "desglose de efectivo:"
    Escribir "billetes de 50000: ", BC
    Escribir "billetes de 20000: ", BV
    Escribir "billetes de 10000: ", BD
    Escribir "billetes de 5000: ", BC2
    Escribir "monedas de 1000: ", BM
    Escribir "cantidad total: ", Total
FinAlgoritmo