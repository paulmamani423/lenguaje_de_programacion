//a. ¿Qué pasaría si no se decrementa al número de obreros en uno?
//b. Realice el mismo algoritmo utilizando la herramienta FOR,
//c. Realice el mismo algoritmo utilizando la herramienta REPEAT.

Algoritmo ejercicio13
    Definir Numero_Obreros, Total_nomina, Numero_Horas_Trabajadas, Salario, i Como Entero;
    Numero_Obreros <- 50;
    Total_nomina <- 0;
    Escribir "nomina de obreros (utilizando for):";
    Escribir "-----------------------------------------"
    Escribir "obrero   |  horas trabajadas  |  salario"
    Escribir "-----------------------------------------"
    Para i <- 1 Hasta Numero_Obreros Hacer;
        Escribir i, "     |     ", Numero_Horas_Trabajadas, "          |       ", Numero_Horas_Trabajadas * 30000;
			Total_nomina <- Total_nomina + (Numero_Horas_Trabajadas * 30000);
        Escribir "ingrese las horas trabajadas para el obrero ", i, ":";
        Leer Numero_Horas_Trabajadas
    FinPara
    Escribir "-----------------------------------------"
    Escribir "total (for): ", Total_nomina
	
    Numero_Obreros <- 50;
    Total_nomina <- 0;
    Escribir "nomina de obreros (utilizando repeat):"
    Escribir "-----------------------------------------"
    Escribir "obrero    |   horas trabajadas   |   salario"
    Escribir "-----------------------------------------"
    Repetir
        Escribir Numero_Obreros, "      |     ", Numero_Horas_Trabajadas, "        |     ", Numero_Horas_Trabajadas * 30000;
			Total_nomina <- Total_nomina + (Numero_Horas_Trabajadas * 30000);
			Numero_Obreros <- Numero_Obreros - 1;
        Escribir "ingrese las horas trabajadas para el obrero ", Numero_Obreros, ":";
        Leer Numero_Horas_Trabajadas
	Hasta Que  Numero_Obreros = 0;
		Escribir "-----------------------------------------"
		Escribir "total (repeat): ", Total_nomina
FinAlgoritmo