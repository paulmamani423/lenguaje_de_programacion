//a. Realiza un algoritmo que le permita determinar el área de un rectángulo.

Algoritmo ejercicio5
	Definir R, H, Volumen, Area, Base, Altura como Real
	
    Escribir "ingrese el valor del radio (R) del cilindro:"
    Leer R
    Escribir "ingrese el valor de la altura (H) del cilindro:"
    Leer H
    Volumen <- 3.1416 * R^2 * H
    Area <- (2 * 3.1416 * R * H) + (2 * 3.1416 * R^2)
    Escribir "el area del cilindro es:", Area
    Escribir "el volumen del cilindro es:", Volumen
	
	Escribir "ingrese el valor de la base del rectangulo:"
    Leer Base
    Escribir "ingrese el valor de la altura del rectangulo:"
    Leer Altura
    Area <- Base * Altura
    Escribir "el area del rectangulo es:", Area
FinAlgoritmo