//Realice el mismo algoritmo utilizando la herramienta IF-THEN – ELSE.

Algoritmo ejercicio14
	Definir Codigo_Producto Como Cadena
    Definir Precio, Subtotal, Total, IVA Como Real
	
    Subtotal <- 0;
    Total <- 0;
    Escribir "ingrese el codigo de producto y precio (deje vacio para finalizar):";
    Repetir
        Leer Codigo_Producto
        Si Codigo_Producto <> "" Entonces;
            Leer Precio
            Subtotal <- Subtotal + Precio;
            Escribir Codigo_Producto, "     ", Precio
        FinSi
	Hasta Que  Codigo_Producto = "";
		IVA <- Subtotal * 0.15;
		Total <- Subtotal + IVA;
		Escribir "-----------------------------------------"
		Escribir "Sub Total: ", Subtotal;
		Escribir "IVA: ", IVA;
		Escribir "Total: ", Total;
FinAlgoritmo
