//a. ¿Qué falta en este algoritmo? ¿ Qué errores presenta?
//b. Desarrollar un algoritmo que calcule el area de un cuadrado.

Algoritmo ejercicio4
		Definir CatA, CatB, Hipotenusa, lado, area como Real;
		
		Escribir "ingrese la longitud del primer cateto (CatA):"
		Leer CatA
		Escribir "ingrese la longitud del segundo cateto (CatB):"
		Leer CatB
		Hipotenusa <- Raiz(CatA^2 + CatB^2)
		Escribir "la hipotenusa del triangulo rectangulo es:", Hipotenusa
		
		Escribir "ingrese la longitud del lado del cuadrado:"
		Leer lado
		area <- lado * lado
		Escribir "el area del cuadrado es:", area
FinAlgoritmo