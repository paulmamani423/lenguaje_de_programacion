//a. Desarrolle un algoritmo que lea la velocidad en metros por segundo y la convierta a kilómetros por
//hora.

Algoritmo ejercicio11
	Definir Vel_ms, Vel_kmh Como Real;
    Escribir "ingrese la velocidad en metros por segundo:";
    Leer Vel_ms;
    Vel_kmh <- (Vel_ms * 3600) / 1000;
    Escribir "la velocidad en kilometros por hora es:", Vel_kmh;
FinAlgoritmo