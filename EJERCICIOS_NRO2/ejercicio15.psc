//a. Desarrolle el algoritmo anterior utilizando la herramienta Repetir (REPEAT).
//b. Desarrolle el algoritmo anterior utilizando la herramienta Si-Entonces-De lo contrario-Fin_SI (IF-THEN-ELSE-
//END-IF)

Algoritmo ejercicio15
    Definir Num_dias, Años, Meses, Semanas, Dias Como Entero
	
    Años <- 0;
    Meses <- 0;
    Semanas <- 0;
    Dias <- 0;
    Escribir "ingrese el numero de dias:"
    Leer Num_dias
    Repetir
        Si Num_dias >= 365 Entonces;
            Años <- Años + 1
            Num_dias <- Num_dias - 365
        Sino
		Si Num_dias >= 12 Entonces;
			Meses <- Meses + 1
			Num_dias <- Num_dias - 30
		Sino
			Si Num_dias >= 7 Entonces;
				Semanas <- Semanas + 1
				Num_dias <- Num_dias - 7
			Sino
				Dias <- Num_dias
				Num_dias <- 0
				FinSi
			FinSi
		FinSi
	Hasta Que  Num_dias = 0;
	
		Escribir "en ", Num_dias, " dias hay:"
		Escribir "años: ", Años
		Escribir "meses: ", Meses
		Escribir "semanas: ", Semanas
		Escribir "dias: ", Dias
		Si Num_dias >= 365 Entonces;
			Años <- Num_dias / 365
			Num_dias <- Num_dias % 365
		FinSi
		Si Num_dias >= 30 Entonces;
			Meses <- Num_dias / 30
			Num_dias <- Num_dias % 30
		FinSi
		Si Num_dias >= 7 Entonces;
			Semanas <- Num_dias / 7
			Num_dias <- Num_dias % 7
		FinSi
		Dias <- Num_dias
		Escribir "en ", Num_dias, " dias hay:"
		Escribir "años: ", Años
		Escribir "meses: ", Meses
		Escribir "semanas: ", Semanas
		Escribir "dias: ", Dias
FinAlgoritmo
