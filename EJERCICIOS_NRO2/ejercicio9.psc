//a. ¿Qué falta en este algoritmo? ¿ Qué errores presenta?
//b. Realice un algoritmo que determine los veinte primeros números, ¿Cuáles son múltiplos de 2?.
//c. Realice un algoritmo que determine cuantos minutos hay en 5 horas.

Algoritmo ejercicio9
		Definir N, J, S como Entero
		J <- 2; S <- 0
		Escribir "ingrese un numero entero positivo:"
		Leer N
		Mientras J <= N / 2 Hacer
		Si N MOD J = 0 Entonces
			S <- S + 1
			Fin Si
			J <- J + 1
		Fin Mientras
		Si S = 0 Entonces
			Escribir N, "es primo"
		Sino
			Escribir N, "no es primo"
		Fin Si
FinAlgoritmo