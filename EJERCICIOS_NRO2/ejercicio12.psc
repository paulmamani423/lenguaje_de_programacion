//a. Desarrolle un algoritmo que permita calcular la media aritmética.

Algoritmo ejercicio12
	Definir N, Suma, Cuenta, Promedio Como Entero
	
    N <- 0; Suma <- 0; Cuenta <- 0; Promedio <- 0;
	
    Escribir "ingrese un numero (0 para terminar):";
    Leer N;
    Mientras N <> 0 Hacer;
        Suma <- Suma + N
        Cuenta <- Cuenta + 1
        Escribir "ingrese otro numero (0 para terminar):";
        Leer N
    Fin Mientras
    Si Cuenta > 0 Entonces;
        Promedio <- Suma / Cuenta
        Escribir "promedio:", Promedio;
    Sino
        Escribir "no se han ingresado datos.";
    Fin Si
FinAlgoritmo