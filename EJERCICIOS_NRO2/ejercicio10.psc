//a. Realice un algoritmo que determine el pago a realizar por la entrada a un espectáculo donde se pueden
//comprar sólo hasta cuatro entrada, donde al costo de dos entradas se les descuenta el 10%, al de tres
//entrada el 15% y a la compra de cuatro tickets se le descuenta el 20 %.

Algoritmo ejercicio10
	
    Definir HE, HS, Pago, HoraEstadia, HoraFraccion, HoraRestante como Entero
	
    Escribir "ingrese la hora de entrada (formato militar):"
    Leer HE
    Escribir "ingrese la hora de salida (formato militar):"
    Leer HS
    HoraEstadia <- HS - HE
    Si HoraEstadia >= 1 Entonces
        HoraFraccion <- HoraEstadia - 1
        Si HoraFraccion >= 1 Entonces
            HoraEstadia <- HoraEstadia + 1
        Fin Si
        HoraRestante <- HoraEstadia - 1
        Pago <- 1000 + (HoraRestante * 600)
    Sino
        Pago <- 1000
    Fin Si
	
    Escribir "el monto a pagar por el servicio de estacionamiento es:", Pago, "bolivares"
    Definir Entradas, Descuento, PrecioTotal como Real
    Escribir "ingrese la cantidad de entradas a comprar:"
    Leer Entradas
    Segun Entradas Hacer
        2:
            Descuento <- 0.1
        3:
            Descuento <- 0.15
        4:
            Descuento <- 0.2
        De Otro Modo:
            Descuento <- 0
    Fin Segun
    PrecioTotal <- Entradas * 1000
    PrecioTotal <- PrecioTotal - (PrecioTotal * Descuento)
    Escribir "el monto a pagar por las", Entradas, "entradas es:", PrecioTotal, "bolivares"
Fin Algoritmo