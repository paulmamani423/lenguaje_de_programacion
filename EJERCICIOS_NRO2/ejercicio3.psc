//a. Desarrolle un algoritmo que realice la sumatoria de los números enteros múltiplos de 5, comprendidos
//entre el 1 y el 100, es decir, 5 + 10 + 15 +.... + 100. El programa deberá imprimir los números en
//cuestión y finalmente su sumatoria
//b. Desarrolle un algoritmo que realice la sumatoria de los números enteros pares comprendidos entre el 1
//y el 100, es decir, 2 + 4 + 6 +.... + 100. El programa deberá imprimir los números en cuestión y
//finalmente su sumatoria
//c. Desarrolle un algoritmo que lea los primeros 300 números enteros y determine cuántos de ellos son
//impares; al final deberá indicar su sumatoria.

Algoritmo ejercicio3
	Definir numero, contador, suma_multiplos_5, suma_pares, impares, suma_impares como Entero;
	
	contador <- 0
	suma_multiplos_5 <- 0
	suma_pares <- 0
	impares <- 0
	suma_impares <- 0
	
	Escribir "sumatoria de n�meros m�ltiplos de 5 entre 1 y 100:"
	Para numero <- 1 Hasta 100 Hacer;
		Si numero MOD 5 = 0 Entonces;
			Escribir numero
			suma_multiplos_5 <- suma_multiplos_5 + numero;
		FinSi
	FinPara
	Escribir "la suma de los n�meros m�ltiplos de 5 es: ", suma_multiplos_5
	
	Escribir "sumatoria de n�meros pares entre 1 y 100:"
	Para numero <- 1 Hasta 100 Hacer;
		Si numero MOD 2 = 0 Entonces;
			Escribir numero
			suma_pares <- suma_pares + numero;
		FinSi
	FinPara
	Escribir "la suma de los n�meros pares es: ", suma_pares
	
	Escribir "cantidad y sumatoria de n�meros impares entre 1 y 300:"
	Para contador <- 1 Hasta 300 Hacer;
		Si contador MOD 2 <> 0 Entonces;
			Escribir contador
			impares <- impares + 1
			suma_impares <- suma_impares + contador
		FinSi
	FinPara
	Escribir "la cantidad de n�meros impares es: ", impares
	Escribir "la suma de los n�meros impares es: ", suma_impares
FinAlgoritmo