//a. Complete el algoritmo con la instrucción o instrucciones necesarias.
//b. Desarrolle un algoritmo que le permita determinar de una lista de números:
//b.1. ¿Cuántos están entre el 50 y 75, ambos inclusive?
//b.2. ¿Cuántos mayores de 80?
//b.3. ¿Cuántos menores de 30?
//El algoritmo debe finalizar cuando n (el total de números de la lista), sea igual a 0.

Algoritmo ejercicio6
	Definir N, Numero, Cont50a75, ContMayor80, ContMenor30 como Entero;
	
    Escribir "ingrese un n�mero:"
    Leer N
	
    Si (N MOD 2 == 0) Entonces;
        Escribir "es par"
    Sino
        Escribir "es impar"
    FinSi
	
	Cont50a75 <- 0
    ContMayor80 <- 0
    ContMenor30 <- 0
	
    Repetir
        Escribir "ingrese un n�mero (0 para finalizar):"
        Leer Numero
		
        Si (Numero <> 0) Entonces;
            Si (Numero >= 50 Y Numero <= 75) Entonces;
                Cont50a75 <- Cont50a75 + 1;
            FinSi
			
            Si (Numero > 80) Entonces;
                ContMayor80 <- ContMayor80 + 1;
            FinSi
			
            Si (Numero < 30) Entonces;
                ContMenor30 <- ContMenor30 + 1;
            FinSi
        FinSi
    Hasta Que (Numero = 0);
	
    Escribir "Cantidad de n�meros entre 50 y 75:", Cont50a75;
    Escribir "Cantidad de n�meros mayores de 80:", ContMayor80;
    Escribir "Cantidad de n�meros menores de 30:", ContMenor30;
FinAlgoritmo